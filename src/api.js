export function fetchInitialData () {
  return fetch('/initial-data.json').then(response => response.json())
}
