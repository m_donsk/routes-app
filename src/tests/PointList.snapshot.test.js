import React from 'react';
import renderer from 'react-test-renderer';
import { getInitialAppState } from './helpers';

import PointList from "../components/PointList";

describe('<PointList />', () => {
  it('checks if PointList matches snapshot', () => {
    const { points } = getInitialAppState();

    const rendered = renderer.create(<PointList points={points} />);

    expect(rendered.toJSON()).toMatchSnapshot();
  });
});
