import React from 'react';
import renderer from 'react-test-renderer';
import { getPointListItemProps } from './helpers';

import PointListItem from "../components/PointListItem";

describe('<PointListItem />', () => {
  it('checks if PointListItem matches snapshot', () => {
    const props = getPointListItemProps();

    const rendered = renderer.create(<PointListItem {...props} />);

    expect(rendered.toJSON()).toMatchSnapshot();
  });

  it('checks if PointListItem with opened address matches snapshot', () => {
    const props = getPointListItemProps();

    const rendered = renderer.create(<PointListItem {...props} />);
    const testInstance = rendered.root;

    // Toggle address visibility
    testInstance.findByProps({className: 'point-list-item__toggle-address-btn'}).props.onClick();

    expect(rendered.toJSON()).toMatchSnapshot();
  });
});
