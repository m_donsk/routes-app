import React from 'react';
import renderer from 'react-test-renderer';

jest.mock('../api');
import App from '../components/App';

describe('<App />', () => {
  it('checks if App matches snapshot', () => {
    const rendered = renderer.create(<App />);

    expect(rendered.toJSON()).toMatchSnapshot();
  });
});