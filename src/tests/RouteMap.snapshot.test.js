import React from 'react';
import renderer from 'react-test-renderer';
import { getInitialAppState } from './helpers';

import RouteMap from '../components/RouteMap';

describe('<RouteMap />', () => {
  it('checks if RouteMap matches snapshot', () => {
    const { points } = getInitialAppState();
    const handleMapMove = jest.fn();

    const rendered = renderer.create(<RouteMap points={points} onMapMove={handleMapMove} />);

    // Should be called with the coordinates of Moscow
    expect(handleMapMove).toBeCalledWith([55.76, 37.64]);

    expect(rendered.toJSON()).toMatchSnapshot();
  });
});
