export function getInitialAppState () {
  return {
    points: [
      {
        title: 'Москва',
        id: 'abc',
        coordinates: [55.76, 37.64],
        address: 'Россия, Москва'
      },
      {
        title: 'Хабаровск',
        id: 'def',
        coordinates: [48.46, 135.11],
        address: 'Россия, Хабаровск'
      },
      {
        title: 'Биробиджан',
        id: 'ghi',
        coordinates: [48.46, 135.11],
        address: 'Россия, Биробиджан'
      }
    ],
    currentCenter: [50, 40]
  }
}

export function getPointListItemProps () {
  const item = {
    id: 'abc',
    title: 'Москва',
    address: 'Россия, Москва'
  };

  // Mock react-beautiful-dnd's stuff
  const provided = {
    innerRef: '',
    draggableProps: {},
    dragHandleProps: {}
  };
  const snapshot = {
    isDragging: false
  };

  // Mock click handler
  const handleDelete = jest.fn();

  return {
    item,
    provided,
    snapshot,
    handleDelete
  };
}
