import { getInitialAppState } from './helpers';

import {
  addPoint,
  deletePoint,
  reorderPoints,
  setCurrentCenter,
  getPointIndexById,
  updateAddressOfPoint,
  updateCoordinatesOfPoint
} from '../app-state-functions';

describe('getPointIndexById', () => {
  const state = getInitialAppState();

  it('get index of point', () => {
    let index = getPointIndexById(state, 'def');
    expect(index).toBe(1);

    index = getPointIndexById(state, 'abc');
    expect(index).toBe(0);

    expect(state).toEqual(getInitialAppState());
  });

  it('get index of point with incorrect id', () => {
    expect(() => getPointIndexById(state, 'xyz')).toThrow('Index not found');

    expect(state).toEqual(getInitialAppState());
  });
});

describe('updateAddressOfPoint', () => {
  it('update address of certain point', () => {
    const state = getInitialAppState();
    const result = updateAddressOfPoint(state, 'abc', 'TEST');

    expect(result.points).toEqual([
      {
        title: 'Москва',
        id: 'abc',
        coordinates: [55.76, 37.64],
        address: 'TEST'
      },
      {
        title: 'Хабаровск',
        id: 'def',
        coordinates: [48.46, 135.11],
        address: 'Россия, Хабаровск'
      },
      {
        title: 'Биробиджан',
        id: 'ghi',
        coordinates: [48.46, 135.11],
        address: 'Россия, Биробиджан'
      }
    ]);

    expect(state).toEqual(getInitialAppState());
  });
});

describe('addPoint', () => {
  it('add new point', () => {
    const state = getInitialAppState();
    const result = addPoint(state, 'TEST');

    expect(result.points).toHaveLength(4);
    expect(result.points[3].title).toBe('TEST');
    expect(result.points[3].coordinates).toEqual([50, 40]);

    expect(state).toEqual(getInitialAppState());
  });
});

describe('deletePoint', () => {
  it('delete a point', () => {
    const state = getInitialAppState();
    const result = deletePoint(state, 'abc');

    expect(result.points).toHaveLength(2);
    expect(result.points).toEqual([
      {
        title: 'Хабаровск',
        id: 'def',
        coordinates: [48.46, 135.11],
        address: 'Россия, Хабаровск'
      },
      {
        title: 'Биробиджан',
        id: 'ghi',
        coordinates: [48.46, 135.11],
        address: 'Россия, Биробиджан'
      }
    ]);

    expect(state).toEqual(getInitialAppState());
  });
});

describe('reorderPoints', () => {
  it('swap items', () => {
    const state = getInitialAppState();
    const result = reorderPoints(state, 0, 1);

    expect(result.points).toEqual([
      {
        title: 'Хабаровск',
        id: 'def',
        coordinates: [48.46, 135.11],
        address: 'Россия, Хабаровск'
      },
      {
        title: 'Москва',
        id: 'abc',
        coordinates: [55.76, 37.64],
        address: 'Россия, Москва'
      },
      {
        title: 'Биробиджан',
        id: 'ghi',
        coordinates: [48.46, 135.11],
        address: 'Россия, Биробиджан'
      }
    ]);

    expect(state).toEqual(getInitialAppState());
  })
});

describe('setCurrentCenter', () => {
  it('set to state the center of the map', () => {
    const result = setCurrentCenter([100, 100]);

    expect(result).toEqual({ currentCenter: [100, 100] });
  });
});

describe('updateCoordinatesOfPoint', () => {
  it('update coordinates of a point', () => {
    const state = getInitialAppState();
    const result = updateCoordinatesOfPoint(state, 'abc', [100, 100]);

    expect(result).toEqual({
      points: [
        {
          title: 'Москва',
          id: 'abc',
          coordinates: [100, 100],
          address: 'Россия, Москва'
        },
        {
          title: 'Хабаровск',
          id: 'def',
          coordinates: [48.46, 135.11],
          address: 'Россия, Хабаровск'
        },
        {
          title: 'Биробиджан',
          id: 'ghi',
          coordinates: [48.46, 135.11],
          address: 'Россия, Биробиджан'
        }
      ]
    });

    expect(state).toEqual(getInitialAppState());
  });
});
