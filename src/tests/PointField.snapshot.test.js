import React from 'react';
import renderer from 'react-test-renderer';

import PointField from '../components/PointField';

describe('<PointField />', () => {
  it('checks if PointField matches snapshot', () => {
    const rendered = renderer.create(<PointField />);

    expect(rendered.toJSON()).toMatchSnapshot();
  });
});