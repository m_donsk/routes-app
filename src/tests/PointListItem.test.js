import React from 'react';
import { shallow } from 'enzyme';
import { getPointListItemProps } from './helpers';

import PointListItem from '../components/PointListItem';

function getPointListItemWrapper () {
  const { item, provided, snapshot, handleDelete } = getPointListItemProps();

  const wrapper = shallow(<PointListItem
    item={item}
    provided={provided}
    snapshot={snapshot}
    onDelete={handleDelete}
  />);

  return {
    item,
    wrapper,
    handleDelete
  };
}

describe('<PointListItem />', () => {
  it('test address toggling', () => {
    const { wrapper } = getPointListItemWrapper();

    expect(wrapper.find('.point-list-item__address').length).toBe(1);
    expect(wrapper.find('.point-list-item__address_opened').length).toBe(0);

    wrapper.find('.point-list-item__toggle-address-btn').simulate('click');
    expect(wrapper.find('.point-list-item__address_opened').length).toBe(1);
  });

  it('test delete button handler', () => {
    const { item, wrapper, handleDelete } = getPointListItemWrapper();

    wrapper.find('.point-list-item__delete-btn').simulate('click');
    expect(handleDelete).toBeCalledWith(item.id);
  });
});
