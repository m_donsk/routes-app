import React from 'react';
import { shallow } from 'enzyme';

import PointField from '../components/PointField';

describe('<PointField/>', () => {
  it('tries to enter title of the new field', () => {
    const handleEnter = jest.fn();
    const wrapper = shallow(<PointField onEnter={handleEnter} />);

    wrapper.find('.point-field').simulate('change', { target: { value: ' test ' } });
    expect(handleEnter).not.toBeCalled();

    wrapper.find('.point-field').simulate('keyup', { keyCode: 13, which: 13, key: 'Enter', target: { value: ' test ' }});
    expect(handleEnter).toBeCalledWith('test');
  });
});
