import shortId from 'shortid';

export function getPointIndexById(state, id) {
  const index = state.points.findIndex(item => item.id === id);

  if (index < 0) {
    throw new Error('Index not found');
  }

  return index;
}

export function updateAddressOfPoint(state, id, newAddress) {
  const index = getPointIndexById(state, id);
  const points = Array.from(state.points);

  points[index] = Object.assign({}, points[index], {
    address: newAddress
  });

  return { points };
}

export function addPoint(state, title) {
  return {
    points: state.points.concat({
      title,
      id: shortId.generate(),
      coordinates: state.currentCenter,
      address: ''
    })
  };
}

export function deletePoint(state, id) {
  const index = getPointIndexById(state, id);

  return {
    points: state.points.slice(0, index).concat(state.points.slice(index + 1))
  };
}

export function reorderPoints(state, startIndex, endIndex) {
  const points = Array.from(state.points);

  const [ removed ] = points.splice(startIndex, 1);
  points.splice(endIndex, 0, removed);

  return { points };
}

export function setCurrentCenter(currentCenter) {
  return { currentCenter };
}

export function updateCoordinatesOfPoint(state, id, newCoordinates) {
  const points = Array.from(state.points);
  const index = getPointIndexById(state, id);

  points[index] = Object.assign({}, points[index], {
    coordinates: newCoordinates
  });

  return { points };
}
