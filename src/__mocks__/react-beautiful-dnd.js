import React, { Component } from 'react';

export class DragDropContext extends Component {
  render() {
    return this.props.children;
  }
}

export class Droppable extends Component {
  render() {
    return this.props.children({ innerRef: '' });
  }
}

export class Draggable extends Component {
  render() {
    return this.props.children(
      {
        innerRef: '',
        draggableProps: {},
        dragHandleProps: {}
      },
      {
        isDragging: false
      }
    );
  }
}
