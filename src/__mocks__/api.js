import { getInitialAppState } from '../tests/helpers';

export function fetchInitialData () {
  const { points } = getInitialAppState();

  // Return thenable object instead of the Promise returned by api
  return {
    then: cb => cb(points)
  };
}
