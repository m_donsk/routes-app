import React, { Component } from 'react';

import './PointField.css';

class PointField extends Component {
  handleKeyUp = e => {
    if (e.key === 'Enter') {
      this.props.onEnter(e.target.value.trim());

      e.target.value = '';
    }
  };

  render() {
    return (
      <input
        className="point-field"
        placeholder="Press Enter to add new item"
        onKeyUp={this.handleKeyUp}
        maxLength={160}
      />
    );
  }
}

export default PointField;
