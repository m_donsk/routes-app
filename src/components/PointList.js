import React from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';

import './PointList.css';
import PointListItem from './PointListItem';

function PointList(props) {
  const listItems = props.points.map((item, index) => (
    <Draggable draggableId={item.id} index={index} key={item.id}>
      {(provided, snapshot) => (
        <PointListItem
          item={item}
          provided={provided}
          snapshot={snapshot}
          onDelete={props.onDelete}
        />
      )}
    </Draggable>
  ));

  return (
    <Droppable droppableId="pointList">
      {provided => (
        <ul
          ref={provided.innerRef}
          className="point-list"
        >
          {listItems}
        </ul>
      )}
    </Droppable>
  );
}

export default PointList;
