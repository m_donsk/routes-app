import React, { Component } from 'react';
import { Map, Placemark, Polyline } from 'react-yandex-maps';

import './RouteMap.css';

const initialMapState = { center: [55.76, 37.64], zoom: 10 };

const getIdAndCoordinatesOfPlacemark = function (event) {
  const target = event.get('target');

  return [ target.properties.get('id'), target.geometry.getCoordinates() ];
};

class RouteMap extends Component {
  handleActionEnd = event => this.props.onMapMove(event.get('map').getCenter());

  handlePlacemarkDrag = event => this.props.onPlacemarkMove(...getIdAndCoordinatesOfPlacemark(event));

  handlePlacemarkDragEnd = event => this.props.onPlacemarkMoveEnd(...getIdAndCoordinatesOfPlacemark(event));

  componentDidMount = () => this.props.onMapMove(initialMapState.center);

  render() {
    const points = this.props.points.map(item => {
      return (
        <Placemark
          key={item.id}
          geometry={{
            coordinates: item.coordinates
          }}
          properties={{
            id: item.id,
            balloonContent: item.title
          }}
          options={{
            draggable: true
          }}
          onDrag={this.handlePlacemarkDrag}
          onDragEnd={this.handlePlacemarkDragEnd}
        />
      );
    });

    const routeCoordinates = this.props.points.map(item => item.coordinates);

    return (
      <div className="route-map">
        <Map
          width="100%"
          height="100%"
          state={initialMapState}
          onActionEnd={this.handleActionEnd}
        >
          {points}
          <Polyline
            geometry={{
              coordinates: routeCoordinates
            }}
            options={{
              strokeWidth: 3
            }}
          />
        </Map>
      </div>
    );
  }
}

export default RouteMap;
