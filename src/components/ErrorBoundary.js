import React, { Component } from 'react';

import './ErrorBoundary.css';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasError: false
    };
  }

  componentDidCatch() {
    // @TODO send error report to remote server

    this.setState({
      hasError: true
    });
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className="error-boundary">
          <h1>Something went wrong!</h1>
          <p>Please report this problem to our support: support@example.com</p>
        </div>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
