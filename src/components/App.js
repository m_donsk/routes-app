import React, { Component } from 'react';
import { YMaps } from 'react-yandex-maps';
import { DragDropContext } from 'react-beautiful-dnd';

import {
  addPoint,
  deletePoint,
  reorderPoints,
  setCurrentCenter,
  updateAddressOfPoint,
  updateCoordinatesOfPoint
} from '../app-state-functions';
import { fetchInitialData } from '../api';

import './App.css';
import RouteMap from './RouteMap';
import PointsList from './PointList';
import PointField from './PointField';

let yMapsApi;

const geocodeSessions = {};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      points: [],

      currentCenter: ''
    };
  }

  setAddressOfPoint(id, coordinates) {
    if (yMapsApi) {
      // Register geocode requests for every point to prevent possible race conditions
      const currentGeocodeSession = {};

      geocodeSessions[id] = currentGeocodeSession;

      yMapsApi.geocode(coordinates)
        .then(res => {
          if (currentGeocodeSession === geocodeSessions[id]) {
            this.setState(prevState => {
              return updateAddressOfPoint(prevState, id, res.geoObjects.get(0).properties.get('text'));
            });
          }
        });
    }
  }

  componentDidMount() {
    fetchInitialData().then(points => this.setState({ points }));
  }

  handleEnter = title => this.setState(
    prevState => addPoint(prevState, title),

    // Fetch address of the last added point
    () => this.setAddressOfPoint(this.state.points[this.state.points.length - 1].id, this.state.currentCenter)
  );

  handleDelete = id => this.setState(prevState => deletePoint(prevState, id));

  handleDragEnd = result => {
    if (!result.destination) {
      return;
    }

    this.setState(prevState => reorderPoints(prevState, result.source.index, result.destination.index));
  };

  handleMapMove = centerCoords => this.setState(setCurrentCenter(centerCoords));

  handlePlacemarkMove = (id, coordinates) => {
    this.setState(prevState => updateCoordinatesOfPoint(prevState, id, coordinates));
  };

  handlePlacemarkMoveEnd = (id, coordinates) => this.setAddressOfPoint(id, coordinates);

  handleApiAvailable = ymaps => {
    yMapsApi = ymaps;

    this.state.points.forEach(point => {
      if (point.address === '') {
        this.setAddressOfPoint(point.id, point.coordinates);
      }
    });
  };

  render() {
    return (
      <DragDropContext onDragEnd={this.handleDragEnd}>
        <div className="app">
          <h1 className="app__header">Fun Box Routes</h1>
          <div className="app__content">
            <div className="app__info">
              <PointField onEnter={this.handleEnter} />
              <PointsList
                points={this.state.points}
                onDelete={this.handleDelete}
              />
            </div>
            <div className="app__map">
              <YMaps onApiAvaliable={ymaps => this.handleApiAvailable(ymaps)}>
                <RouteMap
                  points={this.state.points}
                  onMapMove={this.handleMapMove}
                  onPlacemarkMove={this.handlePlacemarkMove}
                  onPlacemarkMoveEnd={this.handlePlacemarkMoveEnd}
                />
              </YMaps>
            </div>
          </div>
        </div>
      </DragDropContext>
    );
  }
}

export default App;
