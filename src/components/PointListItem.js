import classNames from 'classnames';
import React, { Component } from 'react';

import './PointListItem.css';

class PointListItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAddressVisible: false
    };
  }

  handleToggleAddress = () => this.setState(prevState => ({ isAddressVisible: !prevState.isAddressVisible }));

  render() {
    const isAddressBtnDisabled = this.props.item.address === '';

    return (
      <li
        ref={this.props.provided.innerRef}
        className={classNames(
          [ 'point-list-item' ],
          { 'point-list-item_dragging': this.props.snapshot.isDragging }
        )}
        {...this.props.provided.draggableProps}
        {...this.props.provided.dragHandleProps}
      >
        <div className="point-list-item__title">{this.props.item.title}</div>
        <button
          type="button"
          className="point-list-item__delete-btn"
          onClick={() => this.props.onDelete(this.props.item.id)}
        >
          X
        </button>
        <button
          type="button"
          disabled={isAddressBtnDisabled}
          className={classNames(
            [ 'point-list-item__toggle-address-btn' ],
            { 'point-list-item__toggle-address-btn_opened': this.state.isAddressVisible }
          )}
          onClick={this.handleToggleAddress}
        >
          Address
        </button>
        <p className={classNames(
          [ 'point-list-item__address' ],
          { 'point-list-item__address_opened': this.state.isAddressVisible }
        )}>
          {this.props.item.address}
        </p>
      </li>
    );
  }
}

export default PointListItem;
