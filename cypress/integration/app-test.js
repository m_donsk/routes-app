describe('My First Test', function() {
  it('Test adding and remove of the new point', function() {
    cy.viewport(700, 700);

    cy.visit('/');

    cy.contains('Fun Box Routes');

    // Add new point
    cy.get('.point-field').type('new point{enter}').should('have.value', '');

    // New point has been added
    cy.get('.point-list-item:eq(6)').contains('new point')

    // Address is invisible
      .siblings('.point-list-item__address')
        .should('not.be.visible')

    // Show and check the Address
      .siblings('.point-list-item__toggle-address-btn')
        .click()
        .should('have.class', 'point-list-item__toggle-address-btn_opened')
      .siblings('.point-list-item__address')
        .should('be.visible')
        .should('contain', 'Россия, Москва, Сверчков переулок, 8с1')

    // Remove the point
      .siblings('.point-list-item__delete-btn')
        .click()
        .should('not.exist');

    cy.get('.point-list-item:eq(6)').should('not.exist');
  });
});